
import { SkSelectImpl }  from '../../sk-select/src/impl/sk-select-impl.js';

export class AntdSkSelect extends SkSelectImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'select';
    }

    get body() {
        if (! this._body) {
            let body = this.comp.el.querySelector('.ant-select-selection__rendered');
            if (body == null && this.selectionEl) {
                this.selectionEl.insertAdjacentHTML('afterbegin', `
                    <div class="ant-select-selection__rendered">
                        <div unselectable="on" class="ant-select-selection__placeholder" style="display: none; user-select: none;">--</div>
                    </div>
                `);
                body = this.comp.el.querySelector('.ant-select-selection__rendered');
            }
            this._body = body;
        }
        return this._body;
    }

    set body(el) {
        this._body = el;
    }

    get selectEl() {
        if (! this._selectEl) {
            this._selectEl = this.comp.el.querySelector('.ant-select');
        }
        return this._selectEl;
    }

    set selectEl(el) {
        this._selectEl = el;
    }

    get optionsEl() {
        if (! this._optionsEl) {
            this._optionsEl = this.comp.el.querySelector('.ant-select-dropdown-menu');
        }
        return this._optionsEl;
    }

    set optionsEl(el) {
        this._optionsEl = el;
    }

    get dropdownEl() {
        if (! this._dropdownEl) {
            this._dropdownEl = this.comp.el.querySelector('.ant-select-dropdown');
        }
        return this._dropdownEl;
    }

    set dropdownEl(el) {
        this._dropdownEl = el;
    }

    get selectionEl() {
        if (! this._selectionEl) {
            this._selectionEl = this.comp.el.querySelector('.ant-select-selection');
        }
        return this._selectionEl;
    }

    set selectionEl(el) {
        this._selectionEl = el;
    }

    get popupEl() {
        return this.dropdownEl;
    }

    get subEls() {
        return ['body', 'selectEl', 'optionsEl', 'dropdownEl', 'selectionEl'];
    }

    renderSelectedOption(value, title) {
        if (! title) {
            title = value;
        }
        return `<li unselectable="on" class="ant-select-selection__choice" role="presentation" title="${title}" data-value="${value}" style="user-select: none;"><div class="ant-select-selection__choice__content">${title}</div><span class="ant-select-selection__choice__remove"><i aria-label="icon: close" class="anticon anticon-close ant-select-remove-icon"><svg viewBox="64 64 896 896" focusable="false" class="" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 0 0 203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path></svg></i></span></li>`;
    }

    selectForMulti() {
        if (this.comp.selectedValues) {
            let selectedOptionsHtml = '';
            if (Array.isArray(this.comp.selectedValues)) {
                for (let value of this.comp.selectedValues) { // render from array of values
                    selectedOptionsHtml += this.renderSelectedOption(value);
                }
            } else {
                let values = Object.keys(this.comp.selectedValues);
                for (let value of values) { // build from option -> value hashmap
                    selectedOptionsHtml += this.renderSelectedOption(value, values[value]);
                }
            }
            if (this.selectionContainer) {
                this.selectionContainer.innerHTML = '';
                this.selectionContainer.insertAdjacentHTML('beforeend', `
                    ${selectedOptionsHtml}
                    <li class="ant-select-search ant-select-search&#45;&#45;inline"><div class="ant-select-search__field__wrap"><input autocomplete="off" class="ant-select-search__field" value=""><span class="ant-select-search__field__mirror">&nbsp;</span></div></li>
                `);
            } else {
                this.body.insertAdjacentHTML('beforeend', `
                    <ul class="ant-select-selection__container">
                        ${selectedOptionsHtml}
                        <li class="ant-select-search ant-select-search&#45;&#45;inline"><div class="ant-select-search__field__wrap"><input autocomplete="off" class="ant-select-search__field" value=""><span class="ant-select-search__field__mirror">&nbsp;</span></div></li>
                    </ul>
                `);
            }
        }
    }

    restoreState(state) {
        this.clearAllElCache();
        this.body.innerHTML = '';
        this.optionsEl.innerHTML = '';
        //if (state.contentsState.match('.*optgroup.*')) {
            this.comp.innerHTML = '';
            this.comp.insertAdjacentHTML('beforeend', state.contentsState);
        //}
        this.renderOptions();
        if (this.comp.hasAttribute('multiple')) {
            this.selectionEl.classList.add('ant-select-selection--multiple');
            this.selectForMulti();
        } else {
            this.selectionEl.classList.add('ant-select-selection--single');
            let value = this.comp.value || this.comp.getAttribute('value');
            if (value) {
                for (let key of Object.keys(this.options)) {
                    let optionValue = this.valueFromOption(this.options[key]);
                    if (optionValue === value) {
                        this.selectOption(this.options[key]);
                    }
                }
            } else {
                //this.selectOption();
            }
        }
        // :TODO preselect value this.body.value = this.comp.value;
        // :TODO render selected value
        // :TODO bind display options on lick via setting style for optionsEl
    }

    valueFromOption(option) {
        return (option.dataset.value !== undefined) ? option.dataset.value : option.value ? option.value : option.innerHTML;
    }

    selectOption(option) {
        if (this.comp.hasAttribute('multiple')) {
            this.selectionContainer.innerHTML = '';
            if (option) {
                this.comp.selectValue(this.valueFromOption(option));
                this.selectForMulti();
            }
        } else {
            if (option) {
                let selectedLabel = this.comp.el.querySelector('.ant-select-selection-selected-value');
                if (!selectedLabel) {
                    this.body.insertAdjacentHTML('beforeend', `
                        <div class="ant-select-selection-selected-value" title="${option.innerHTML}" style="display: block; opacity: 1;">${option.innerHTML}</div>
                    `);
                    selectedLabel = this.body.querySelector('.ant-select-selection-selected-value');
                }
                selectedLabel.innerHTML = option.innerHTML;
                selectedLabel.setAttribute('title', option.innerHTML);
                this.comp.setAttribute('value', this.valueFromOption(option));
                this.comp.dispatchEvent(new CustomEvent('change', { target: this.comp, bubbles: true, composed: true }));
                this.comp.dispatchEvent(new CustomEvent('skchange', { target: this.comp, bubbles: true, composed: true }));
            } else {
                this.body.innerHTML = '';
                this.body.insertAdjacentHTML('beforeend', `
                        <div class="ant-select-selection-selected-value" style="display: block; opacity: 1;">--</div>
                    `);
/*                let option = this.options['option-1'];
                if (option) {
                    this.comp.setAttribute('value', (option.dataset.value !== undefined)
                        ? option.dataset.value : (option.getAttribute('value') ? option.getAttribute('value') : option.innerHTML));
                    this.body.innerHTML = '';
                    this.body.insertAdjacentHTML('beforeend', `
                        <div class="ant-select-selection-selected-value" title="${option.innerHTML}" style="display: block; opacity: 1;">${option.innerHTML}</div>
                    `);
                }*/
            }
        }
    }

    onClick(event) {
        this.comp.callPluginHook('onEventEnd', event);
        if (this.comp && this.comp.getAttribute('disabled')) {
            this.comp.callPluginHook('onEventEnd', event);
            return false;
        }
        let target = event.originalTarget || event.srcElement || event.target; // firefox and ie
        if (target.classList.contains('ant-select-dropdown-menu-item')) {
            this.selectOption(target);
            this.toggleSelection();
        } else {
            if (event.path) {
                let selectedForCloseItem = this.findCloseItem(event.path);
                let selectedItem = this.findSelectionItem(event.path);
                let value = selectedItem ? selectedItem.dataset.value : null;
                if (selectedForCloseItem) {
                    this.comp.deSelectValue(value);
                    this.selectForMulti();
                } else {
                    if (value) {
                        let selectedOptionKeys = Object.keys(this.options).filter(key => this.options[key].value === value);
                        if (selectedOptionKeys) {
                            this.selectOption(this.options[selectedOptionKeys[0]]);
                        }
                    }
                    this.toggleSelection();
                }

            } else {
                if (target && target.tagName === 'LI') {
                    let value = target.dataset.value;
                    //this.comp.deSelectValue(value);
                    let selectedOptionKeys = Object.keys(this.options).filter(key => this.options[key].value === value);
                    if (selectedOptionKeys) {
                        this.selectOption(this.options[selectedOptionKeys[0]]);
                    }
                    this.toggleSelection();
                } else if (target && target.tagName === 'svg' && target.dataset.icon === 'close') {
                    let value = target.parentElement.parentElement.parentElement.dataset.value;
                    this.comp.deSelectValue(value);
                    this.selectForMulti();
                } else {
                    this.toggleSelection();
                }
            }
        }
        this.comp.callPluginHook('onEventEnd', event);
    }

    toggleSelection() {
        let dropdownEl = this.comp.el.querySelector('.ant-select-dropdown');
        let open = (! dropdownEl.classList.contains('ant-select-dropdown-hidden')) || this.comp.hasAttribute('open');
        if (open) {
            dropdownEl.classList.add('ant-select-dropdown-hidden');
            this.comp.removeAttribute('open');
        } else {
            //let box = this.comp.el.host.getBoundingClientRect();
            let box = this.selectEl.getBoundingClientRect();
            dropdownEl.classList.remove('ant-select-dropdown-hidden');
            dropdownEl.style.position = 'fixed';
            dropdownEl.style.left = box.left + 'px';
            dropdownEl.style.top = (box.bottom) + 'px';
            dropdownEl.style.width = (box.width).toString() + 'px';
            this.comp.setAttribute('open', '');
        }
    }

    findSelectionItem(path) {
        for (let item of path) {
            if (item.tagName === 'LI') {
                return item;
            }
        }
    }

    findCloseItem(path) {
        for (let item of path) {
            if (item.tagName === 'svg' && item.dataset.icon === 'close') {
                return item;
            }
        }
    }

    get selectionContainer() {
        return this.comp.el.querySelector('.ant-select-selection__container');
    }

    bindEvents() {
        super.bindEvents();
        if (this.clickHandle) {
            this.comp.removeEventListener('click', this.clickHandle);
        }
        this.clickHandle = this.onClick.bind(this);
        this.comp.addEventListener('click', this.clickHandle);
    }

    unbindEvents() {
        super.unbindEvents();
        if (this.clickHandle) {
            this.comp.removeEventListener('click', this.clickHandle);
        }
    }

    renderOptions() {
        let num = 1;
        this.options = {};
        let optgroups = this.comp.querySelectorAll('optgroup');
        if (optgroups.length === 0) {
            optgroups = this.body.querySelectorAll('optgroup');
        }
        if (optgroups.length === 0) {
            optgroups = this.comp.optgroups;
        }
        if (optgroups && optgroups.length > 0) {
            if (! this.comp.optgroups) {
                this.comp.optgroups = optgroups;
                for (let optgroup of this.comp.optgroups) {
                    let label = optgroup.getAttribute('label') ? optgroup.getAttribute('label') : '';
                    this.optionsEl.insertAdjacentHTML('beforeend', `
                    <li class="ant-select-dropdown-menu-item-group">
                        <div class="ant-select-dropdown-menu-item-group-title" title="${label}">
                            ${label}
                        </div>
                        <ul class="ant-select-dropdown-menu-item-group-list">`
                    );
                    let groupOptions = optgroup.querySelectorAll('option');
                    for (let option of groupOptions) {
                        this.renderOption(option, num);
                        this.options['option-' + num] = option;
                        num++;
                    }
                    this.optionsEl.insertAdjacentHTML('beforeend', `</ul>
                    </li>
                `);
                    if (optgroup.parentElement) {
                        optgroup.parentElement.removeChild(optgroup);
                    }
                }
            }
        } else {
            let options = this.comp.querySelectorAll('option');
            if (options.length === 0) {
                options = this.body.querySelectorAll('option');
            }
            for (let optionEl of options) {
                this.renderOption(optionEl, num);
                this.options['option-' + num] = optionEl;
                num++;
                if (optionEl.parentElement) {
                    optionEl.parentElement.removeChild(optionEl);
                }
            }
        }
    }

    renderOption(option, num) {
        this.optionsEl.insertAdjacentHTML('beforeend', `
            <li role="option" class="ant-select-dropdown-menu-item" aria-selected="false" 
                style="user-select: none;" data-value="${option.value ? option.value : option.innerHTML}">${option.innerHTML}</li>
        `);
        //option.parentElement.removeChild(option);
    }

    afterRendered() {
        super.afterRendered();
        this.clearAllElCache();
        this.bindFitPopup();
        this.mountStyles();
    }

    enable() {
        this.selectEl.classList.remove('ant-select-disabled');
        this.selectEl.classList.add('ant-select-enabled');
    }

    disable() {
        this.selectEl.classList.add('ant-select-disabled');
        this.selectEl.classList.remove('ant-select-enabled');
    }

    showInvalid() {
        super.showInvalid();
        this.selectEl.classList.add('form-invalid');
    }

    showValid() {
        super.showValid();
        this.selectEl.classList.remove('form-invalid');
    }

    focus() {
        this.selectEl.focus();
    }
}
